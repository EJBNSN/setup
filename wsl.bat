choco install -y wsl-ubuntu-1804
icacls "%ProgramData%\chocolatey\lib\wsl-ubuntu-1804\tools\unzipped" /grant Users:(OI)(CI)F /T
wsl useradd -m -p pass -s /bin/bash %USERNAME%
wsl echo root:pass | wsl chpasswd
wsl echo edwar:pass | wsl chpasswd
wsl usermod -aG sudo edwar
wsl wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh
wsl bash ~/miniconda.sh -b -p /mnt/c/py
wsl /bin/bash -c "echo '. /mnt/c/py/etc/profile.d/conda.sh' >> ~/.bashrc"
REM wsl wget https://bitbucket.org/EJBNSN/setup/raw/master/setup.py -O ~/setup.py
REM wsl ~/py/bin/python ~/setup.py
wsl apt update --fix-missing -y
wsl apt upgrade -y
%ProgramData%\chocolatey\lib\wsl-ubuntu-1804\tools\unzipped\ubuntu1804.exe config --default-user %USERNAME%